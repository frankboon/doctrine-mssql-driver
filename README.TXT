In case php just stops executing when fetching results a cause could be that you have
"mssql.datetimeconvert = On" in your php.ini. This should be changed to "Off"