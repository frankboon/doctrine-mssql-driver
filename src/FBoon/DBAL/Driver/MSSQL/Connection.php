<?php

namespace FBoon\DBAL\Driver\MSSQL;

use Doctrine\DBAL\Driver\Connection as ConnectionInterface;
/**
 * Sqlsrv Connection implementation.
 *
 * @author Frank Boon <boon.frank@gmail.com>
 */
class Connection implements ConnectionInterface
{
    protected $_conn;

    public function __construct(array $params, $username, $password, array $driverOptions = array())
    {
        $port = isset($params['port']) ? ':'.$params['port'] : '';

        $this->_conn = mssql_connect($params['host'].$port, $username, $password);
        mssql_select_db($params['dbname'], $this->_conn);
    }

    /**
     * @override
     */
    public function quote($value, $type=\PDO::PARAM_STR)
    {
        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function beginTransaction()
    {
        mssql_query("BEGIN TRANSACTION");
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function commit()
    {
        return mssql_query("COMMIT");
    }

    /**
     * {@inheritdoc}
     */
    public function errorCode()
    {
        throw new \NotImplementedException('errorCode() is not yet implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function errorInfo()
    {
        throw new \NotImplementedException('errorInfo() is not yet implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function exec($statement)
    {
        $query = mssql_query($statement);

        return $this->_conn->affected_rows;
    }

    /**
     * {@inheritdoc}
     */
    public function lastInsertId($name = null)
    {
        $id = "";

        $rs = mssql_query("SELECT @@identity AS id");
        if ($row = mssql_fetch_row($rs)) {
            $id = trim($row[0]);
        }
        mssql_free_result($rs);

        return $id;
    }

    /**
     * {@inheritdoc}
     */
    public function prepare($prepareString)
    {
        return new Statement($this, $prepareString);
    }

    /**
     * {@inheritdoc}
     */
    public function query()
    {
        $args = func_get_args();
        $sql = $args[0];
        $stmt = $this->prepare($sql);
        $stmt->execute();
        return $stmt;
    }

    public function rollBack()
    {
        return mssql_query("ROLLBACK");
    }
}